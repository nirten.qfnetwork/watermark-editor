const axios = require('axios');
const express = require('express');
const bodyParser = require('body-parser');
const watermark = require('watermark-jimp');
const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const cors = require('cors');
const Jimp = require('jimp');

const writeFile = promisify(fs.writeFile);
const unlink = promisify(fs.unlink);
const app = express();
const port = 4000;

app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors());

app.get('/', async (req, res) => {
    return res.status(400).json({ message: 'Welcome to the watermark editor server' });
});

app.post('/add-watermark', async (req, res) => {
    const { mainImageUrl, watermarkImageBase64, Opacity, rotation, compression } = req.body;

    console.log("Rotation:", rotation);

    if (!mainImageUrl || !watermarkImageBase64) {
        return res.status(400).json({ error: 'Missing required fields' });
    }

    try {
        const WatermarkedImageBase64 = await apply_repetition_and_rotation(watermarkImageBase64, 500, rotation);

        if (!WatermarkedImageBase64) {
            return res.status(400).json({ error: 'Invalid watermark image' });
        }

        const mainImageResponse = await axios.get(mainImageUrl, { responseType: 'arraybuffer' });
        const mainImageBuffer = Buffer.from(mainImageResponse.data, 'binary');
        const mainTempImagePath = path.join(__dirname, 'temp_mainImage.jpg');
        await writeFile(mainTempImagePath, mainImageBuffer);

        const watermarkImageBuffer = Buffer.from(WatermarkedImageBase64.split(',')[1], 'base64');
        const watermarkTempImagePath = path.join(__dirname, 'temp_watermarkedImage.png');
        await writeFile(watermarkTempImagePath, watermarkImageBuffer);

        const date = new Date();
        const outputImagePath = path.join(__dirname, `output-image/output${date.getTime()}.jpg`);

        const watermarkOptions = {
            dstPath: outputImagePath,
            opacity: parseFloat(Opacity) || 0.3,
            rotate: parseInt(rotation) || 0
        };

        await watermark.addWatermark(mainTempImagePath, watermarkTempImagePath, watermarkOptions);

        let imageUrl = "";

        if (compression) {
            // Compress the watermarked image
            const compressedImagePath = await compressImage(outputImagePath);
            imageUrl = `${req.protocol}://${req.get('host')}/output-image/${path.basename(compressedImagePath)}`;

        }
        else {
            imageUrl = `${req.protocol}://${req.get('host')}/output-image/${path.basename(outputImagePath)}`;
        }




        res.status(200).json({ message: 'Watermark added and image compressed successfully', imageUrl });

        await unlink(mainTempImagePath);
        await unlink(watermarkTempImagePath);

        setTimeout(async () => {
            try {
                await unlink(compressedImagePath);
                console.log(`Deleted ${compressedImagePath} after 2 minutes`);
            } catch (err) {
                console.error('Error deleting watermarked image:', err);
            }
        }, 2 * 60 * 1000);

    } catch (err) {
        console.error('Error adding watermark:', err);
        res.status(500).json({ error: 'Error adding watermark', details: err.message });
    }
});

async function apply_repetition_and_rotation(base64, gap, rotation) {
    try {
        const buffer = Buffer.from(base64.split(',')[1], 'base64');
        const image = await Jimp.read(buffer);

        // Rotate the image
        image.rotate(parseInt(rotation), false);

        // Create a new image with transparent background
        const repeatCount = 3; // How many times to repeat the image
        const repeatedWidth = (image.bitmap.width + gap) * repeatCount - gap;
        const repeatedHeight = (image.bitmap.height + gap) * repeatCount - gap;
        const newImage = new Jimp(repeatedWidth, repeatedHeight, 0x00000000);

        // Draw the repeated image with gap
        for (let x = 0; x < repeatCount; x++) {
            for (let y = 0; y < repeatCount; y++) {
                newImage.composite(image, x * (image.bitmap.width + gap), y * (image.bitmap.height + gap));
            }
        }

        // Get the base64 representation of the new image
        const newBase64Image = await newImage.getBase64Async(Jimp.MIME_PNG);

        return newBase64Image;
    } catch (error) {
        console.error('Error processing the image:', error);
        return null;
    }
}

async function compressImage(imagePath) {
    const image = await Jimp.read(imagePath);
    const compressedImagePath = imagePath.replace(/\.jpg$/, '_compresseddfsd.jpg');
    await image.quality(60).writeAsync(compressedImagePath);
    return compressedImagePath;
}

app.post('/upload-to-server', async (req, res) => {
    const { base64Image } = req.body;

    if (!base64Image) {
        return res.status(400).json({ error: 'No image provided' });
    }

    try {
        const imageBuffer = Buffer.from(base64Image.split(',')[1], 'base64');
        const fileName = `image-${Date.now()}.png`;
        const filePath = path.join(__dirname, 'uploads', fileName);

        if (!fs.existsSync(path.join(__dirname, 'uploads'))) {
            fs.mkdirSync(path.join(__dirname, 'uploads'));
        }

        await writeFile(filePath, imageBuffer);

        const imageUrl = `${req.protocol}://${req.get('host')}/uploads/${fileName}`;

        res.status(200).json({ url: imageUrl });
    } catch (error) {
        console.error('Error uploading image:', error);
        res.status(500).json({ error: 'Error uploading image' });
    }
});

app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use('/scripts', express.static(path.join(__dirname, 'scripts')));
app.use('/output-image', express.static(path.join(__dirname, 'output-image')));

app.listen(port, () => {
    console.log(`Server running on http://localhost:${port}`);
});


// (async () => {

//     console.log("compressed image",(await compressImage("C:/Users/DELL/Downloads/download.png")));
// })()
